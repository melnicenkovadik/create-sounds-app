window.addEventListener("load", () => {
  const sounds = document.querySelectorAll(".sound");
  const pads = document.querySelectorAll(".pads div");
  const slants = document.querySelectorAll(".slants div");
  const visual = document.querySelector(".visual");
  const pad1 = document.querySelectorAll(".pads .pad");

  const colors = [
    "#60d394",
    "#d36060",
    "#c060d3",
    "#d3d160",
    "#606bd3",
    "#60c2d3"
  ];


  pads.forEach((pad, index) => {
    console.log(index);

    document.addEventListener('keydown', function (event) {
      if (event.code === 'KeyZ') {
        sounds[0].currentTime = 0;
        sounds[0].play();
        createBubble(0);
      }
      if (event.code === 'KeyX') {
        sounds[1].currentTime = 0;
        sounds[1].play();
        createBubble(1);
      }
      if (event.code === 'KeyC') {
        sounds[2].currentTime = 0;
        sounds[2].play();
        createBubble(2);
      }
      if (event.code === 'KeyV') {
        sounds[3].currentTime = 0;
        sounds[3].play();
        createBubble(3);
      }
      if (event.code === 'KeyB') {
        sounds[4].currentTime = 0;
        sounds[4].play();
        createBubble(4);
      }
      if (event.code === 'KeyN') {
        sounds[5].currentTime = 0;
        sounds[5].play();
        createBubble(5);
      }


    })
    pad.addEventListener("click", function () {
      sounds[index].currentTime = 0;
      sounds[index].play();
      createBubble(index);
    });
  });


  const createBubble = index => {
    //Create bubbles
    const bubble = document.createElement("div");
    visual.appendChild(bubble);
    bubble.style.backgroundColor = colors[index];
    bubble.style.animation = `jump 1s ease`;
    bubble.addEventListener("animationend", function () {
      visual.removeChild(this);
    });
  }

})
;

